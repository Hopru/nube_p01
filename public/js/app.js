var currentIndex = 0;
var items;
$(document).ready(function() {
  $("#version").html("v0.14");
  
  $("#searchbutton").click( function (e) {
    displayModal();
  });
  
  $("#searchfield").keydown( function (e) {
    if(e.keyCode == 13) {
      displayModal();
    }	
  });
  
  function displayModal() {
    $(  "#myModal").modal('show');

    $("#status").html("Searching...");
    $("#dialogtitle").html("Search for: "+$("#searchfield").val());
    $("#previous").hide();
    $("#next").hide();
    $.getJSON('/search/' + $("#searchfield").val() , function(data) {
		console.log("Receiving JSONNNNNN");
		console.log(JSON.stringify(data));
      renderQueryResults(data);
    });
  }
  
  $("#next").click( function(e) {
		currentIndex = currentIndex + 4;
		render();
  });
  
  $("#previous").click( function(e) {
		currentIndex = currentIndex - 4;
		render();
  });

  function renderQueryResults(data) {
    items = data;
    if (data.error != undefined) {
      	$("#status").html("Error: "+data.error);
    } else {
      	$("#status").html(""+data.num_results+" result(s)");
		console.log("Rendering items");
		console.log(JSON.stringify(data));
		render();
	}
  }
	function render() {
		var j;
		for(j = currentIndex; j < currentIndex + 4; j++) {
			if(j + 1 > items.num_results) {
				$("#photo" + (j%4)).html("No Picture");
			} else {
				console.log("Adding " + items.results[j]);
				$("#photo" + (j%4)).html('<img id="theImg" width="200px" height="100px" src="' + items.results[j] + '"/>');
			}
		}
		
		if(currentIndex - 4 >= 0) {
			$("#previous").show();
		} else {
			$("#previous").hide();
		}
		if(currentIndex + 4 + 1 < items.num_results) {
		$("#next").show();
		} else {
			$("#next").hide();
		}
	}
});
