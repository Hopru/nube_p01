  var AWS = require('aws-sdk');
  AWS.config.loadFromPath('./config.json');

  var db = new AWS.DynamoDB();

  function keyvaluestore(table) {
    this.LRU = require("lru-cache");
    this.cache = this.LRU({ max: 500 });
    this.tableName = table;
  };

  /**
   * Initialize the tables
   * 
   */
  keyvaluestore.prototype.init = function(whendone) {
    
    var tableName = this.tableName;
    var self = this;
	
	var params = {
		TableName: tableName
	   };
	   db.describeTable(params, function(err, data) {
		 if (err) {

			 console.log(err, err.stack); // an error occurred
		 }
		 else {
			console.log(JSON.stringify(data));  
			 whendone();
			
		 }         // successful response
	
	   });
  };

  /**
   * Get result(s) by key
   * 
   * @param search
   * 
   * Callback returns a list of objects with keys "inx" and "value"
   */
  
keyvaluestore.prototype.get = function(search, callback) {
    var self = this;
    
    if (self.cache.get(search))
          callback(null, self.cache.get(search));
    else {
		
		var items = [];
		var params = {
			ExpressionAttributeValues: {
			 ":v1": {
			   S: search
			  }
			},
			Limit: 1000,
			KeyConditionExpression: "mykey = :v1", 
			
			TableName: this.tableName
		   };
		   db.query(params, function(err, data) {
			 if (err) console.log(err, err.stack); // an error occurred
			 else {
				console.log(JSON.stringify(data));           // successful response
				 for(var i=0; i<data.Count; i++) {
					items.push({"inx": data.Items[i].inx.N, "value": data.Items[i].myvalue.S, "key": data.Items[i].mykey});
				 }
				 self.cache.set(search, items);
				 callback(err, items);
			 }
		   });
        
      /*
       * 
       * La función QUERY debe generar un arreglo de objetos JSON son cada
       * una de los resultados obtenidos. (inx, value, key).
       * Al final este arreglo debe ser insertado al cache. Y llamar a callback
       * 
       * Ejemplo:
       *    var items = [];
       *    items.push({"inx": data.Items[0].inx.N, "value": data.Items[0].value.S, "key": data.Items[0].key});
       *    self.cache.set(search, items)
       *    callback(err, items);
       */
    }
  };


  module.exports = keyvaluestore;
